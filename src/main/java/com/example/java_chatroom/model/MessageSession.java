package com.example.java_chatroom.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: LENOVO
 * Date: 2024-04-15
 * Time: 18:06
 */
/*public class MessageSession {
    private int sessionId;
    private List<Friend> friends;
    private String lastMessage;

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public List<Friend> getFriends() {
        return friends;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }
}*/

public class MessageSession {
    private int sessionId;
    private List<Friend> friends;
    private String lastMessage;

    // 构造函数、getter 和 setter 方法...

    // 添加一个字段表示会话类型，用于区分是个人会话还是群聊会话
    private SessionType sessionType;

    // 枚举类型表示会话类型（个人会话或群聊会话）
    public enum SessionType {
        PERSONAL, // 个人会话
        GROUP     // 群聊会话
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public List<Friend> getFriends() {
        return friends;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public SessionType getSessionType() {
        return sessionType;
    }

    public void setSessionType(SessionType sessionType) {
        this.sessionType = sessionType;
    }
}

