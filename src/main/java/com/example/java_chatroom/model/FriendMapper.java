package com.example.java_chatroom.model;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: LENOVO
 * Date: 2024-04-15
 * Time: 13:00
 */
@Mapper
public interface FriendMapper {
    List<Friend> selectFriendList(int userId);

    // 找出包含 friendName 的用户信息. 抛出掉 selfUserId 本身和他的好友.
    List<Friend> findFriend(int selfUserId, String friendName);

    // 保存添加好友的请求
    void addFriendRequest(int fromUserId, int toUserId, String reason);

    // 获取添加好友的请求(可能有多个，所以是一组集合)
    List<AddFriendRequest> getFriendRequest(int toUserId);

    // 删除之前的添加好友的请求
    void deleteFriendRequest(int fromUserId, int toUserId);

    // 在好友列表中新增一项
    void addFriend(int userId, int friendId);

}
