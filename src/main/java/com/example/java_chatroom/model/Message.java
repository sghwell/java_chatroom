package com.example.java_chatroom.model;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: LENOVO
 * Date: 2024-04-16
 * Time: 13:25
 */
/*public class Message {
    private int messageId;
    private int fromId; // 表示发送者用户 id
    private String fromName; // 表示发送者的用户名
    private int sessionId;
    private String content;

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public int getFromId() {
        return fromId;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}*/
public class Message {
    private int messageId;
    private int fromId; // 表示发送者用户 id
    private String fromName; // 表示发送者的用户名
    private int sessionId;
    private String content;

    // 构造函数、getter 和 setter 方法...

    // 添加一个字段表示消息类型，用于区分是群聊消息还是个人会话消息
    private MessageType messageType;

    // 枚举类型表示消息类型（个人会话或群聊）
    public enum MessageType {
        PERSONAL, // 个人会话消息
        GROUP     // 群聊消息
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public int getFromId() {
        return fromId;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }
}

