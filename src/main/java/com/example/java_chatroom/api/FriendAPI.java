package com.example.java_chatroom.api;

import com.example.java_chatroom.component.OnlineUserManager;
import com.example.java_chatroom.model.AddFriendRequest;
import com.example.java_chatroom.model.Friend;
import com.example.java_chatroom.model.FriendMapper;
import com.example.java_chatroom.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: LENOVO
 * Date: 2024-04-15
 * Time: 13:36
 */
@RestController
public class FriendAPI {

    @Resource
    private FriendMapper friendMapper;

    @Resource
    private OnlineUserManager onlineUserManager;

    @Resource
    private ObjectMapper objectMapper;

    @GetMapping("/friendList")
    @ResponseBody
    public Object getFriendList(HttpServletRequest req) {
        // 1.先从会话中，获取到userId是啥。
        HttpSession session = req.getSession(false);
        if (session == null) {
            // 不存在，即未登录
            System.out.println("[getFriendList] session 不存在");
            return new ArrayList<Friend>();
        }
        User user = (User) session.getAttribute("user");
        if (user == null) {
            // 用户对象不在会话中存在
            System.out.println("[getFriendList] user 不存在");
            return new ArrayList<Friend>();
        }

        // 根据userId从数据库查数据即可
        List<Friend> friendList = friendMapper.selectFriendList(user.getUserId());
        return friendList;
    }

    @GetMapping("/findFriend")
    @ResponseBody
    public Object findFriend(String friendName, HttpServletRequest req) {
        //1.先从会话中获取到userId
        HttpSession session = req.getSession(false);
        if (session == null) {
            //会话不存在，用户未登录，直接返回一个空列表
            System.out.println("[findFriend] 当前获取不到 session 对象！");
            return new ArrayList<Friend>();
        }
        User user = (User) session.getAttribute("user");
        if (user == null) {
            //当前用户对象没在会话中存在
            System.out.println("[FindFriend] 当前获取不到 user 对象！");
            return new ArrayList<Friend>();
        }
        //2.根据 userId 和 friendName 从数据库查询数据即可
        return friendMapper.findFriend(user.getUserId(), friendName);
    }

    @GetMapping("/addFriend")
    @ResponseBody
    public Object addFriend(int friendId, String reason, HttpServletRequest req) throws IOException {
        //1.先从会话中获取到userId
        HttpSession session = req.getSession(false);
        if (session == null) {
            //会话不存在，用户未登录，直接返回一个空列表
            System.out.println("[addFriend] 当前获取不到 session 对象！");
            return "";
        }
        User user = (User) session.getAttribute("user");
        if (user == null) {
            //当前用户对象没在会话中存在
            System.out.println("[addFriend] 当前获取不到 user 对象！");
            return "";
        }
        //2.将添加好友请求保存到到数据库
        System.out.println("用户[" + user.getUserId() +"]请求添加用户[" + friendId +"为好友");
        friendMapper.addFriendRequest(user.getUserId(), friendId, reason);
        //3.获取被添加的用户在线状态
        WebSocketSession webSocketSesion = onlineUserManager.getSession(friendId);
        if (webSocketSesion != null) {//被添加的用户在线
            AddFriendRequest addFriendRequest = new AddFriendRequest();
            addFriendRequest.setType("addFriend");
            addFriendRequest.setFromUserId(user.getUserId());
            addFriendRequest.setFromUserName(user.getUsername());
            addFriendRequest.setReason(reason);
            //就直接实时发送添加好友请求(websocket请求)给他，提示有用户添加好友
            webSocketSesion.sendMessage(new TextMessage(objectMapper.writeValueAsString(addFriendRequest)));
         }
        return "";
    }

    @GetMapping("/getFriendRequest")
    @ResponseBody
    public Object getFriendRequest(HttpServletRequest req) {
        //1.先从会话中获取到userId
        HttpSession session = req.getSession(false);
        if (session == null) {
            //会话不存在，用户未登录，直接返回一个空列表
            System.out.println("[getFriendRequest] 当前获取不到 session 对象！");
            return new ArrayList<AddFriendRequest>();
        }
        User user = (User) session.getAttribute("user");
        if (user == null) {
            //当前用户对象没在会话中存在
            System.out.println("[getFriendRequest] 当前获取不到 user 对象！");
            return new ArrayList<AddFriendRequest>();
        }
        //2.查询该用户的所有的好友请求有哪些
        return friendMapper.getFriendRequest(user.getUserId());
    }

    @GetMapping("/acceptFriend")
    @ResponseBody
    @Transactional
    public Object acceptFriend(int friendId, HttpServletRequest req) throws IOException {
        //1.先从会话中获取到userId
        HttpSession session = req.getSession(false);
        if (session == null) {
            //会话不存在，用户未登录，直接返回一个空列表
            System.out.println("[acceptFriend] 当前获取不到 session 对象！");
            return "";
        }
        User user = (User) session.getAttribute("user");
        if (user == null) {
            //当前用户对象没在会话中存在
            System.out.println("[acceptFriend] 当前获取不到 user 对象！");
            return "";
        }
        //2.删除好友请求表的存档
        friendMapper.deleteFriendRequest(friendId, user.getUserId());
        //3.修改好友表
        friendMapper.addFriend(friendId, user.getUserId());
        //4.获取添加本人好友的在线状态
        WebSocketSession webSocketSession = onlineUserManager.getSession(friendId);
        if (webSocketSession != null) {//如果他在线
            AddFriendRequest addFriendRequest = new AddFriendRequest();
            addFriendRequest.setType("acceptFriend");
            addFriendRequest.setFromUserId(user.getUserId());
            addFriendRequest.setFromUserName(user.getUsername());
            //那么他会实时知道我接受了好友请求（自动弹窗）(通过 websocket 通知对方并且刷新好友列表)
            webSocketSession.sendMessage(new TextMessage(objectMapper.writeValueAsString(addFriendRequest)));
        }
        return "";
    }


    @GetMapping("/rejectFriend")
    @ResponseBody
    @Transactional
    public Object rejectFriend(int friendId, HttpServletRequest req) throws IOException {
        //1.先从会话中获取到userId
        HttpSession session = req.getSession(false);
        if (session == null) {
            //会话不存在，用户未登录，直接返回一个空列表
            System.out.println("[rejectFriend] 当前获取不到 session 对象！");
            return "";
        }
        User user = (User) session.getAttribute("user");
        if (user == null) {
            //当前用户对象没在会话中存在
            System.out.println("[rejectFriend] 当前获取不到 user 对象！");
            return "";
        }
        //2.删除好友请求表的存档
        friendMapper.deleteFriendRequest(friendId, user.getUserId());
        //3.获取添加本人好友的在线状态
        WebSocketSession webSocketSession = onlineUserManager.getSession(friendId);
        if (webSocketSession != null) {//如果他在线
            AddFriendRequest addFriendRequest = new AddFriendRequest();
            addFriendRequest.setType("rejectFriend");
            addFriendRequest.setFromUserId(user.getUserId());
            addFriendRequest.setFromUserName(user.getUsername());
            //那么他会实时知道我拒绝了好友请求（自动弹窗）(通过 websocket 通知对方)
            webSocketSession.sendMessage(new TextMessage(objectMapper.writeValueAsString(addFriendRequest)));
        }
        return "";
    }
}
